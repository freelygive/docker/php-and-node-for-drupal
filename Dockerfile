ARG PHP_OS
ARG PHP_IMAGE
ARG NODE_OS
ARG NODE_IMAGE

# Build from the specified node image.
FROM node:${NODE_IMAGE}-${NODE_OS} as node

# Build from the specified PHP image.
FROM php:${PHP_IMAGE}-${PHP_OS} as php

# Copy in node's /usr/local for node/npm etc.
COPY --from=node /usr/local/ /usr/local/

# Use the specified configuration.
ARG PHP_INI_VER=production
RUN mv "$PHP_INI_DIR/php.ini-$PHP_INI_VER" "$PHP_INI_DIR/php.ini"

# Create a PHP-CLI configuration with no memory limit.
RUN cp "$PHP_INI_DIR/php.ini" "$PHP_INI_DIR/php-cli.ini" \
    && sed -i 's/memory_limit = .*/memory_limit = -1/' "$PHP_INI_DIR/php-cli.ini"

# Install additional dependencies.
RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
        git \
        unzip \
        mariadb-client \
        rsync \
        openssh-client \
        zstd
        
# Add the php extension installer.
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions

# Install additional php dependencies.
RUN install-php-extensions zip gd mysqli pdo_mysql bcmath bz2 intl soap

# Install Composer.
ARG COMPOSER_VER=2
RUN curl -sS https://getcomposer.org/installer | php -- --${COMPOSER_VER} --install-dir=/usr/local/bin --filename=composer
RUN if [ "${COMPOSER_VER}" -eq "1" ]; then composer global require hirak/prestissimo; fi

# Install Gulp CLI.
RUN npm install --global gulp-cli

# Allow bower to run as root.
RUN echo '{ "allow_root": true }' > /root/.bowerrc
